import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

var config = {
    apiKey: "AIzaSyAwfKNDFyCr3zQFk-zAW0993EGw1hU1jno",
    authDomain: "magic-agency.firebaseapp.com",
    databaseURL: "https://magic-agency.firebaseio.com",
    projectId: "magic-agency",
    storageBucket: "magic-agency.appspot.com",
    messagingSenderId: "783405097683"
};
firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;